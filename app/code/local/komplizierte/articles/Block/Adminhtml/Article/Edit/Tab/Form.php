<?php
/**
 * komplizierte_articles extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       komplizierte
 * @package        komplizierte_articles
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Article edit form tab
 *
 * @category    komplizierte
 * @package     komplizierte_articles
 * @author      Ultimate Module Creator
 */
class komplizierte_articles_Block_Adminhtml_Article_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return komplizierte_articles_Block_Adminhtml_Article_Edit_Tab_Form
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('article_');
        $form->setFieldNameSuffix('article');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'article_form',
            array('legend' => Mage::helper('komplizierte_articles')->__('Article'))
        );
        $fieldset->addType(
            'image',
            Mage::getConfig()->getBlockClassName('komplizierte_articles/adminhtml_article_helper_image')
        );
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

        $fieldset->addField(
            'title',
            'text',
            array(
                'label' => Mage::helper('komplizierte_articles')->__('Title'),
                'name'  => 'title',
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'text',
            'editor',
            array(
                'label' => Mage::helper('komplizierte_articles')->__('Text'),
                'name'  => 'text',
            'config' => $wysiwygConfig,
            'required'  => true,
            'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'image',
            'image',
            array(
                'label' => Mage::helper('komplizierte_articles')->__('Image'),
                'name'  => 'image',

           )
        );

        $fieldset->addField(
            'enabled',
            'text',
            array(
                'label' => Mage::helper('komplizierte_articles')->__('Enabled'),
                'name'  => 'enabled',
            'required'  => true,
            'class' => 'required-entry',

           )
        );
        $fieldset->addField(
            'url_key',
            'text',
            array(
                'label' => Mage::helper('komplizierte_articles')->__('Url key'),
                'name'  => 'url_key',
                'note'  => Mage::helper('komplizierte_articles')->__('Relative to Website Base URL')
            )
        );
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => Mage::helper('komplizierte_articles')->__('Status'),
                'name'   => 'status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('komplizierte_articles')->__('Enabled'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('komplizierte_articles')->__('Disabled'),
                    ),
                ),
            )
        );
        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_article')->setStoreId(Mage::app()->getStore(true)->getId());
        }
        $formValues = Mage::registry('current_article')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getArticleData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getArticleData());
            Mage::getSingleton('adminhtml/session')->setArticleData(null);
        } elseif (Mage::registry('current_article')) {
            $formValues = array_merge($formValues, Mage::registry('current_article')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
