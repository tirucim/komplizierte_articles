<?php
/**
 * komplizierte_articles extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       komplizierte
 * @package        komplizierte_articles
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Article admin edit tabs
 *
 * @category    komplizierte
 * @package     komplizierte_articles
 * @author      Ultimate Module Creator
 */
class komplizierte_articles_Block_Adminhtml_Article_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('article_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('komplizierte_articles')->__('Article'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return komplizierte_articles_Block_Adminhtml_Article_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_article',
            array(
                'label'   => Mage::helper('komplizierte_articles')->__('Article'),
                'title'   => Mage::helper('komplizierte_articles')->__('Article'),
                'content' => $this->getLayout()->createBlock(
                    'komplizierte_articles/adminhtml_article_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        $this->addTab(
            'form_meta_article',
            array(
                'label'   => Mage::helper('komplizierte_articles')->__('Meta'),
                'title'   => Mage::helper('komplizierte_articles')->__('Meta'),
                'content' => $this->getLayout()->createBlock(
                    'komplizierte_articles/adminhtml_article_edit_tab_meta'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_article',
                array(
                    'label'   => Mage::helper('komplizierte_articles')->__('Store views'),
                    'title'   => Mage::helper('komplizierte_articles')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'komplizierte_articles/adminhtml_article_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve article entity
     *
     * @access public
     * @return komplizierte_articles_Model_Article
     * @author Ultimate Module Creator
     */
    public function getArticle()
    {
        return Mage::registry('current_article');
    }
}
