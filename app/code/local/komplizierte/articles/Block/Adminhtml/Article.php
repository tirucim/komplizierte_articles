<?php
/**
 * komplizierte_articles extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       komplizierte
 * @package        komplizierte_articles
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Article admin block
 *
 * @category    komplizierte
 * @package     komplizierte_articles
 * @author      Ultimate Module Creator
 */
class komplizierte_articles_Block_Adminhtml_Article extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_article';
        $this->_blockGroup         = 'komplizierte_articles';
        parent::__construct();
        $this->_headerText         = Mage::helper('komplizierte_articles')->__('Article');
        $this->_updateButton('add', 'label', Mage::helper('komplizierte_articles')->__('Add Article'));

    }
}
