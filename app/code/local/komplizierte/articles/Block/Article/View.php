<?php
/**
 * komplizierte_articles extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       komplizierte
 * @package        komplizierte_articles
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Article view block
 *
 * @category    komplizierte
 * @package     komplizierte_articles
 * @author      Ultimate Module Creator
 */
class komplizierte_articles_Block_Article_View extends Mage_Core_Block_Template
{
    /**
     * get the current article
     *
     * @access public
     * @return mixed (komplizierte_articles_Model_Article|null)
     * @author Ultimate Module Creator
     */
    public function getCurrentArticle()
    {
        return Mage::registry('current_article');
    }
}
