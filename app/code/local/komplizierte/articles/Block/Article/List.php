<?php
/**
 * komplizierte_articles extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       komplizierte
 * @package        komplizierte_articles
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Article list block
 *
 * @category    komplizierte
 * @package     komplizierte_articles
 * @author Ultimate Module Creator
 */
class komplizierte_articles_Block_Article_List extends Mage_Core_Block_Template
{
    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $articles = Mage::getResourceModel('komplizierte_articles/article_collection')
                         ->addStoreFilter(Mage::app()->getStore())
                         ->addFieldToFilter('status', 1);
        $articles->setOrder('enabled', 'asc');
        $this->setArticles($articles);
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return komplizierte_articles_Block_Article_List
     * @author Ultimate Module Creator
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock(
            'page/html_pager',
            'komplizierte_articles.article.html.pager'
        )
        ->setCollection($this->getArticles());
        $this->setChild('pager', $pager);
        $this->getArticles()->load();
        return $this;
    }

    /**
     * get the pager html
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
